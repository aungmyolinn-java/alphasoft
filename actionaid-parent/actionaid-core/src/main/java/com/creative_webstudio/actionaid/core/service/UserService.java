package com.creative_webstudio.actionaid.core.service;

import java.util.List;

import com.creative_webstudio.actionaid.core.domain.User;

/**
 * 
 * @author LanyonM
 */
public interface UserService {

	/**
	 * @return a list of all {@link User}s
	 */
	public List<User> getAllUsers();
	/**
	 * @param user
	 * @return success
	 */
	public boolean saveUser(User user);

}
