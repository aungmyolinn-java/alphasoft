package com.creative_webstudio.actionaid.core.mapper;

import java.util.List;

import com.creative_webstudio.actionaid.core.domain.User;

public interface UserMapper {

	/**
	 * @return all the users
	 */
	List<User> getAllUsers();
	/**
	 * @param user
	 * @return the number of rows affected
	 */
	int insertUser(User user);
	/**
	 * @param user
	 * @return the number of rows affected
	 */
	int updateUser(User user);
}
