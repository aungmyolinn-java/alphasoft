package com.creative_webstudio.actionaid.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.creative_webstudio.actionaid.core.domain.User;
import com.creative_webstudio.actionaid.core.service.UserService;

@RestController
public class HomeController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<User> users() {
		return userService.getAllUsers();
	}

}
