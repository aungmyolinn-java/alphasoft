package com.creative_webstudio.actionaid.core.domain;

import java.io.Serializable;

/**
 * @author Aung Myo Linn
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String userName;
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
